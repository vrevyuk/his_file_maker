var rimraf = require("rimraf");
const fs = require('fs');
const promisify = require('util.promisify');
const fsExists = promisify(fs.exists);
const fsMkdir = promisify(fs.mkdir);
const fsRead = promisify(fs.readFile);
const fsWrite = promisify(fs.writeFile);
const fsUnlink = promisify(fs.unlink);
const fsUnlinkDir = promisify(rimraf);
const {spawn} = require('child_process');
const {loadImage, createCanvas, createImageData, registerFont} = require('canvas');

let [firts = '1000', last = '10000'] = process.argv.slice(2);

async function prepareImage(code, imageFileName) {

    let templateImage = await fsRead(`${__dirname}/template_images/smslist2.png`);
    let image = await loadImage(templateImage);

    return await new Promise(function(resolve, reject) {
        try {
            let canvas = createCanvas(image.width, image.height);
            let ctx = canvas.getContext('2d');
            ctx.beginPath();
            ctx.rect(0, 0, image.width, image.height);
            ctx.fillStyle = "#FF0000";
            ctx.fill();

            ctx.drawImage(image, 0, 0, image.width, image.height);

            ctx.font = '130px Impact';
            let textSize = ctx.measureText(code);
            ctx.fillText(code, 900, 315);

            pipeImage = fs.createWriteStream(imageFileName);
            let canvasOut = canvas.createPNGStream();
            canvasOut.pipe(pipeImage);
            canvasOut.on('end', function(error) {
                error ? reject(error) : resolve(imageFileName);
            })
        } catch(e) {
            reject(e)
        }
    });
}

async function makeFile(position, maxNumber) {

	if (position + 1 < maxNumber) {

        console.log(`Starting ${position} ...`);
		let workDir = `${__dirname}/movies/${position}`;
		if (await fsExists(workDir)) {
            return makeFile(position + 1, maxNumber);
        }    

        await fsMkdir(workDir, {recursive: true});
		await prepareImage(position, `${workDir}/image.png`);

		let cmd = 'ffmpeg';
        let args = [
            '-loop', '1', '-i', `${workDir}/image.png`, 
            '-c:v', 'libx264', '-profile:v', 'main', '-level', '3.1', '-pix_fmt', 'yuv420p', '-vf', 'scale=1280:720', 
            '-t', '180', '-hls_playlist_type', 'vod', '-y', `${workDir}/index.m3u8`
        ];

		let proc = spawn(cmd, args);

		proc.on('close', function(code) {
			console.log('\tclose', code, `width code ${code}`)
            setTimeout(makeFile.bind(null, position + 1, maxNumber), 0);
		});
		proc.on('error', err => console.log('ffmpeg error', err));

	} else {
        console.log('done');
    };
}

console.log(`Makes code from ${firts} to ${last}`);
makeFile(parseInt(firts, 10), parseInt(last, 10));